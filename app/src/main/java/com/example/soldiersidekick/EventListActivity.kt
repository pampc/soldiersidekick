package com.example.soldiersidekick

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.firestore.FirebaseFirestore
import java.util.*
import androidx.core.app.NotificationCompat
import android.app.PendingIntent
import android.app.TaskStackBuilder
import androidx.core.app.ComponentActivity
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import com.google.firebase.firestore.Query
import kotlinx.android.synthetic.main.activity_event_list.*
import kotlin.collections.ArrayList

class EventListActivity : AppCompatActivity() {

    // userID intent variable.
    var userID = ""

    // event coordinates
    var eventCoords = mapOf<String, String>()

    // list of events by id.
    var eventID: MutableList<String> = ArrayList()

    // event credentials for list adapter
    var title: MutableList<String> = ArrayList()
    var instructor: MutableList<String> = ArrayList()
    var orders: MutableList<String> = ArrayList()
    var desc: MutableList<String> = ArrayList()
    var location: MutableList<String> = ArrayList()
    var startDate: MutableList<Date> = ArrayList()
    var endDate: MutableList<Date> = ArrayList()

    // variable for array adapter to hold events and display them using array adapter.
    var attendingList: ArrayList<String> = ArrayList()

    // current date
    val c = Calendar.getInstance().time

    // general tag for debugging/error logging
    private val TAG = "EventListActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_event_list)

        // firestore instance
        val db = FirebaseFirestore.getInstance()

        userID = intent.extras?.getString("userID").toString()

        // check to see if events is a collection in firestore, once that is done, check if soldiers is a collection as well.
        db.collection("events").orderBy("createdDate", Query.Direction.DESCENDING).get().addOnCompleteListener { taskEvent ->
            if (taskEvent.isSuccessful) {
                db.collection("soldiers").document(userID).get()
                    .addOnCompleteListener { taskSoldiers ->
                        if (taskSoldiers.isSuccessful) {
                            for (document in taskEvent.result!!) {

                                val startTime = document.get("startDate") as com.google.firebase.Timestamp
                                val endTime = document.get("endDate") as com.google.firebase.Timestamp

                                // We check soldiers collection to see if attendingEvents array is empty, if its empty tell soldier to go back and sign up for something
                                if (taskSoldiers.getResult()?.get("attendingEvents") == null) {
                                    Log.d(TAG, "This soldier is not attending anything")
                                    Toast.makeText(
                                        this@EventListActivity,
                                        "This soldier has no upcoming events, go back and sign up!",
                                        Toast.LENGTH_LONG
                                    ).show()

                                } else if (c > startTime.toDate()) {
                                    Toast.makeText(
                                        this@EventListActivity,
                                        "Choose the event you want to track (if applicable).",
                                        Toast.LENGTH_LONG
                                    ).show()
                                } else {
                                    // turn attendingEvents array into List
                                    var soldierDB =
                                        taskSoldiers.getResult()?.get("attendingEvents") as List<String>
                                    for (attending in soldierDB) {
                                        Log.d(TAG, "$attending")
                                        // if the soldier is attending an event, add the event to list, get the event attributes and display them dynamically.
                                        if (document.id == attending) {
                                            eventID.add(document.id) // get the event id for deciding which markers show in MapsActivity

                                            title.add(document.get("title").toString())
                                            instructor.add(document.get("instructor").toString())
                                            orders.add(document.get("orders").toString())
                                            desc.add(document.get("exerciseDescription").toString())
                                            location.add(document.get("location").toString())
                                            startDate.add(startTime.toDate())
                                            endDate.add(endTime.toDate())

                                            var events =
                                                "\n${startTime.toDate()} - ${endTime.toDate()}\n\n" + document.get(
                                                    "title"
                                                ) + "\n" + "Instructor: " + document.get("instructor") + "\n" +
                                                        "Location: " + document.get("location") + "\n" + "\n" +
                                                        document.get("exerciseDescription") + "\n"

                                            attendingList.add(events)

                                            // Create an ArrayAdapter from List
                                            var arrayAdapter = ArrayAdapter(
                                                this@EventListActivity,
                                                android.R.layout.simple_list_item_1,
                                                attendingList
                                            )

                                            // display array adapter in ListView
                                            eventList.adapter = arrayAdapter

                                            // if the start date is empty for some reason, do not continue with on click listener to avoid null exception.
                                            if (startDate.toString().isEmpty()) {
                                                Log.e(TAG, "Null string for event start date.")
                                            } else {
                                                eventList.onItemClickListener =
                                                    AdapterView.OnItemClickListener { parent, v, position, id ->

                                                        eventCoords = document.get("coordinates") as Map<String, String>

                                                        // create bundle for map activity with eventID and userID, for easy document reference.
                                                        val extras = Bundle()

                                                        // Intent for the activity to open when user selects the notification
                                                        val detailsIntent = Intent(
                                                            this@EventListActivity,
                                                            EventDetailsActivity::class.java
                                                        )

                                                        extras.putString(
                                                            "userID",
                                                            userID
                                                        )
                                                        extras.putString(
                                                            "eventID",
                                                            eventID[position]
                                                        )
                                                        extras.putString(
                                                            "startDate",
                                                            startDate[position].toString()
                                                        )
                                                        extras.putString(
                                                            "endDate",
                                                            endDate[position].toString()
                                                        )
                                                        extras.putString(
                                                            "title",
                                                            title[position]
                                                        )
                                                        extras.putString(
                                                            "instructor",
                                                            instructor[position]
                                                        )
                                                        extras.putString(
                                                            "orders",
                                                            orders[position]
                                                        )
                                                        extras.putString(
                                                            "desc",
                                                            desc[position]
                                                        )
                                                        extras.putString(
                                                            "location",
                                                            location[position]
                                                        )
                                                        extras.putString(
                                                            "eventLat",
                                                            eventCoords["lat"].toString()
                                                        )
                                                        extras.putString(
                                                            "eventLng",
                                                            eventCoords["lng"].toString()
                                                        )

                                                        detailsIntent.putExtras(extras)

                                                        startActivity(detailsIntent)

                                                    }
                                            }
                                        }
                                    }
                                }

                            }
                        } else {
                            Log.e(TAG, "get failed with ", taskSoldiers.exception)
                        }
                    }
            } else {
                Log.e(TAG, "get failed with ", taskEvent.exception)
            }
        }
    }
}
