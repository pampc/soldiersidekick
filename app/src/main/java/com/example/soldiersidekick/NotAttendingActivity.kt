package com.example.soldiersidekick

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import kotlinx.android.synthetic.main.activity_not_attending.*
import kotlinx.android.synthetic.main.attend_list_custom.*
import java.util.*

class NotAttendingActivity : AppCompatActivity() {

    // List of events to display using array adapter
    var eventList: ArrayList<String> = ArrayList()

    // array adapter used to display event documents in ListView
    lateinit var arrayAdapterFBEvents: ArrayAdapter<String>

    // general tag for debugging/error logging
    private val SUCCESS = "Success"
    private val FAIL = "Fail"

    val c: Date = Calendar.getInstance().time

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_not_attending)

        val userID = intent.extras?.getString("userID").toString()

        //Access the firestore
        val db = FirebaseFirestore.getInstance()

        db.collection("soldiers").document(userID).get()
            .addOnCompleteListener { taskSoldiers ->
                if (taskSoldiers.isSuccessful) {

                    var notAttending = taskSoldiers.result?.get("notAttending") as ArrayList<String>

                    //.orderBy("createdDate", Query.Direction.DESCENDING)

                    db.collection("events").orderBy("createdDate", Query.Direction.DESCENDING).get().addOnSuccessListener { taskEvent ->
                        for (document in taskEvent) {
                            val startTime =
                                document.get("startDate") as com.google.firebase.Timestamp
                            val endTime =
                                document.get("endDate") as com.google.firebase.Timestamp

                            val startDate = startTime.toDate()
                            val endDate = endTime.toDate()
                            if (notAttending.contains(document.id) && c < startDate) {
                                var event =
                                    "\n$startDate - $endDate\n\n" + document.get(
                                        "title"
                                    ) + "\n" + "Instructor: " + document.get("instructor") + "\n" +
                                            "Location: " + document.get("location") + "\n" + "\n" +
                                            document.get("exerciseDescription") + "\n"

                                eventList.add(event)
                            }
                            else { //do not add anything to list
                            }
                        }

                        // create adapter and display events using the event list
                        arrayAdapterFBEvents = ArrayAdapter(
                            this@NotAttendingActivity,
                            android.R.layout.simple_list_item_1,
                            eventList
                        )

                        lstNotAttending.adapter = arrayAdapterFBEvents

                        lstNotAttending.onItemClickListener =
                            AdapterView.OnItemClickListener { parent, v, position, id ->
                                // on item click, create a dialog that handles attending and not attending functionality.
                                val alert =
                                    AlertDialog.Builder(this@NotAttendingActivity)

                                // message is the event details from the list of event details above
                                alert.setMessage(eventList[position])
                                alert.setTitle("Attend this Event?")

                                // attend button listener
                                alert.setPositiveButton("Attend") { _, _ ->
                                    try {
                                        Toast.makeText(
                                            this@NotAttendingActivity,
                                            "Attending Confirmed",
                                            Toast.LENGTH_SHORT
                                        ).show()

                                        // update the 'attending' array in events document by pushing the users ID into it.
                                        db.collection("events")
                                            .document(notAttending[position])
                                            .update(
                                                "attending",
                                                FieldValue.arrayUnion(userID)
                                            )
                                            .addOnSuccessListener {
                                                Log.d(
                                                    SUCCESS,
                                                    "DocumentSnapshot successfully written!"
                                                )
                                            }
                                            .addOnFailureListener { e ->
                                                Log.w(
                                                    FAIL,
                                                    "Error writing document",
                                                    e
                                                )
                                            }
                                    } catch (e: Exception) {//on success and on fail listeners above handle the exceptions
                                    }

                                    // also update the soldiers 'attendingEvents' array with event ID
                                    db.collection("soldiers").document(userID)
                                        .update(
                                            "attendingEvents",
                                            FieldValue.arrayUnion(notAttending[position])
                                        )
                                        .addOnSuccessListener {
                                            Log.d(
                                                SUCCESS,
                                                "DocumentSnapshot successfully written!"
                                            )
                                        }
                                        .addOnFailureListener { e ->
                                            Log.w(
                                                FAIL,
                                                "Error writing document",
                                                e
                                            )
                                        }

                                    // Not attending events get removed from not attending list
                                    db.collection("soldiers").document(userID)
                                        .update(
                                            "notAttending",
                                            FieldValue.arrayRemove(notAttending[position])
                                        )
                                        .addOnSuccessListener {
                                            Log.d(
                                                SUCCESS,
                                                "DocumentSnapshot successfully written!"
                                            )
                                        }
                                        .addOnFailureListener { e ->
                                            Log.w(
                                                FAIL,
                                                "Error writing document",
                                                e
                                            )
                                        }

                                    // Remove the event from the array adapter
                                    eventList.removeAt(position)
                                    notAttending.removeAt(position)

                                    //notify adapter that there was a change in the list it is using.
                                    arrayAdapterFBEvents.notifyDataSetChanged()
                                }

                                alert.setNegativeButton("Cancel") { _, _ -> }

                                alert.show()
                            }
                    }
                }
            }
    }
}

