package com.example.soldiersidekick

import android.Manifest.permission.CAMERA
import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.annotation.SuppressLint
import android.app.Activity
import android.content.ContentResolver
import android.content.ContentValues
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.telephony.PhoneNumberUtils
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.auth.EmailAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageMetadata
import com.google.firebase.storage.UploadTask

import kotlinx.android.synthetic.main.activity_edit.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_password_reset.*
import kotlinx.android.synthetic.main.content_edit.*
import java.util.regex.Matcher
import java.util.regex.Pattern

class EditActivity : AppCompatActivity() {

    private val IMAGE_CAPTURE_CODE = 1001
    private val PERMISSION_CODE = 1000
    var image_uri: Uri? = null
    //userID intent variable
    var userID = ""
    lateinit var db: FirebaseFirestore
    private lateinit var auth: FirebaseAuth

    val storageRef =
        FirebaseStorage.getInstance("gs://prototypetesting-9e43d.appspot.com/").reference

    // Generic Log tags for success and fail on authentication
    private val SUCCESS = "Success"
    private val FAIL = "Fail"

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit)
        setSupportActionBar(toolbar)

        userID = intent.extras?.getString("userID").toString()

        // firestore instance
        db = FirebaseFirestore.getInstance()

        db.collection("soldiers").document(userID).get().addOnCompleteListener { taskSoldiers ->
            if (taskSoldiers.isSuccessful) {
                edtPFirstName.setText(taskSoldiers.result?.get("firstName").toString())
                edtPLastName.setText(taskSoldiers.result?.get("lastName").toString())
                edtPPhone.setText(taskSoldiers.result?.get("phoneNumber").toString())
            }
        }
    }

    fun confirmEditBtn(view: View) {

        // If none of the boxes are empty, create the firestore map object with the credentials and update the soldiers document.
        if (edtPFirstName.text.isNotEmpty() && edtPLastName.text.isNotEmpty()
            && edtPPhone.text.isNotEmpty()
        ) {
            // validations for soldier number and phone number.
            if (PhoneNumberUtils.isGlobalPhoneNumber(edtPPhone.text.toString()) && edtPPhone.text.length == 10) {

                db.collection("soldiers").document(userID).update(
                    mapOf(
                        "firstName" to edtPFirstName.text.toString(),
                        "lastName" to edtPLastName.text.toString(),
                        "name" to "${edtPFirstName.text} ${edtPLastName.text}",
                        "phoneNumber" to edtPPhone.text.toString()
                    )
                ).addOnSuccessListener {
                    Log.d(
                        SUCCESS,
                        "DocumentSnapshot successfully written!"
                    )
                }.addOnFailureListener { e ->
                    Log.w(
                        FAIL,
                        "Error writing document",
                        e
                    )
                }
                // if connection was successful and no exceptions were called
                Toast.makeText(this, "Success!", Toast.LENGTH_SHORT).show()

                val intent = Intent(this, DashboardActivity::class.java)
                intent.putExtra("userID", userID)
                // put userID back into intent and finish this activity.
                finish()
            } else {
                edtPPhone.error = "Input a 10 digit phone number please."
            }
        } else {
            Toast.makeText(this, "Please fill in all of the fields", Toast.LENGTH_LONG).show()
        }
    }

    fun resetBtn(view: View) {
        // reset password button
        val intent = Intent(this, PasswordResetActivity::class.java)
        intent.putExtra("userID", userID)
        startActivity(intent)
        finish()
    }

    fun resetEmailBtn(view: View) {
        // reset email button
        val intent = Intent(this, EmailResetActivity::class.java)
        intent.putExtra("userID", userID)
        startActivity(intent)
        finish()
    }

    override fun onBackPressed() {
        // go back to parent activity with userID
        val intent = Intent(this, DashboardActivity::class.java)
        intent.putExtra("userID", userID)

        super.onBackPressed()
    }

    fun takePhoto(view: View) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(CAMERA) == PackageManager.PERMISSION_DENIED ||
                checkSelfPermission(WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED
            ) {

                val permission = arrayOf(CAMERA, WRITE_EXTERNAL_STORAGE)

                //SHOW POPUP REQUEST PERMISSION
                requestPermissions(permission, PERMISSION_CODE)
            } else {
                openCamera()
            }
        } else {
            //system os is < marshmallow
            openCamera()
        }
    }

    private fun openCamera() {
        val values = ContentValues()
        values.put(MediaStore.Images.Media.TITLE, "404")
        values.put(MediaStore.Images.Media.DESCRIPTION, "From the Camera")

        image_uri = contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)

        //camera intent
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, image_uri)
        startActivityForResult(cameraIntent, IMAGE_CAPTURE_CODE)
    }

    // handle permission request
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            PERMISSION_CODE -> {
                if (grantResults.size > 0 && grantResults[0]
                    == PackageManager.PERMISSION_GRANTED
                ) {
                    //PERMISSION WAS GRANTED
                    openCamera()
                } else {
                    //permission from popup was denied
                    Toast.makeText(this, "Permission Denied", Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    @SuppressLint("MissingSuperCall")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            image_view.setImageURI(image_uri)

            image_view.layoutParams.height = 400
            image_view.layoutParams.width = 400

            image_view.scaleType = ImageView.ScaleType.FIT_XY

            upload.visibility = View.VISIBLE
        }
    }


    fun upload(view: View) {
        if (image_uri != null) {
            // Create the file metadata
            val metadata = StorageMetadata.Builder()
                .setContentType("image/jpeg")
                .build()

            // Upload file and metadata to the path '/404s/'
            val uploadTask =
                storageRef.child("/404s/${userID + "-404"}").putFile(image_uri!!, metadata)

            // Listen for state changes, errors, and completion of the upload.
            uploadTask.addOnProgressListener { taskSnapshot ->
                val progress =
                    (100.0 * taskSnapshot.bytesTransferred) / taskSnapshot.totalByteCount
                println("Upload is $progress% done")
            }.addOnPausedListener {
                println("Upload is paused")
            }.addOnFailureListener {
                // Handle unsuccessful uploads
                Toast.makeText(this, "Upload was Unsuccessful for image", Toast.LENGTH_LONG)
                    .show()
            }.addOnSuccessListener {
                // Handle successful uploads on complete
                Toast.makeText(this, "Upload was SUCCESSFUL for image", Toast.LENGTH_LONG)
                    .show()
            }
        }

    }

}
