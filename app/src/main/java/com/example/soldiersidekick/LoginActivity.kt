package com.example.soldiersidekick


import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_login.btnLogin

class LoginActivity: AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    private val TAG = "LoginActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_login)

        // Initialize Firebase Auth
        auth = FirebaseAuth.getInstance()

        val intent = Intent(this, DashboardActivity::class.java)

        btnLogin.setOnClickListener {

            // Fire base auth to create a user with email and password
            if(edtLoginEmail.text.isNotEmpty() && edtLoginPassword.text.isNotEmpty()) {
                // perform firebase auth login
                val email = edtLoginEmail.text.toString()
                val password = edtLoginPassword.text.toString()

                auth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this) { task ->
                        if (task.isSuccessful) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "LogInWithEmail:success")
                            intent.putExtra("userID", task.result!!.user!!.uid)

                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            startActivity(intent)

                            finish()

                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "createUserWithEmail:failure", task.exception)
                            Toast.makeText(
                                baseContext, "Authentication failed.",
                                Toast.LENGTH_SHORT
                            ).show()
                        }

                    }

                // return to the registration page
            }
            else {
                Toast.makeText(this, "Please fill in the Email and Password fields.", Toast.LENGTH_LONG).show()
            }
        }

        btnBackToRegistration.setOnClickListener {
            this.finish()
        }
    }
}