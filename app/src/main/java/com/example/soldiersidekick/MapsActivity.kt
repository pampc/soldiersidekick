package com.example.soldiersidekick

import android.Manifest
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Location
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Looper
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.soldiersidekick.Models.MapMarker
import com.example.soldiersidekick.R.*
import com.google.android.gms.location.*

import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.*
import com.google.firebase.firestore.DocumentChange
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.SetOptions
import kotlinx.android.synthetic.main.activity_maps.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class MapsActivity : AppCompatActivity(), OnMapReadyCallback,
    GoogleMap.OnInfoWindowClickListener {

    // mMap object to be used for map throughout the app
    private var mMap: GoogleMap? = null

    // variable to hold the soldiers name for solider CallSigns
    private var soldierName: String = ""

    // used in determining the users current location
    internal lateinit var mLocationRequest: LocationRequest
    // last known location stored in mLastLocation
    internal lateinit var mLastLocation: Location
    // current location
    internal var mCurrLocationMarker: Marker? = null
    // used in determining the users current location
    internal var mFusedLocationClient: FusedLocationProviderClient? = null

    // used to see if any markers are added to the map for firebase
    internal var allPoints: MutableList<MapMarker>? = ArrayList()

    // latitude and longitude of specific event
    internal var eventLat = ""
    internal var eventLng = ""

    // the map object that represents the map_request documents added to firebase
    internal var mapRequest: MutableMap<String, Any> = HashMap()

    // request variables
    internal lateinit var requests: MutableList<String>

    internal var requestsList: ArrayList<String> = ArrayList()
    // type is always generic for pins in soldier app (for now)
    private val type = "generic"

    private val ERROR = "error"
    private val SUCCESS = "Test Success"
    private val FAIL = "Test Error"
    private val TAG = "MapsActivity"

    //intent variables
    var userID = ""
    var eventID = ""

    val db = FirebaseFirestore.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layout.activity_maps)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager.findFragmentById(id.map) as SupportMapFragment

        //get values from past activity for the event fields and the user ID
        val intent = intent
        val extras = intent.extras

        userID = extras?.getString("userID").toString()
        eventID = extras?.getString("eventID").toString()
        eventLat = extras?.getString("eventLat").toString()
        eventLng = extras?.getString("eventLng").toString()

        // Location client for checking location permissions, and gps current locations.
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        mapFragment.getMapAsync(this) // onMapReadyCallback

        deletePinsBtn.setOnClickListener {
            if (allPoints!!.isNotEmpty()) {
                allPoints!!.clear()
                mMap!!.clear()

                db.collection("map_request").whereEqualTo("eventId", eventID).get()
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            for (document in task.result!!) {
                                val typeMarker = mMap!!.addMarker(
                                    MarkerOptions().position(
                                        LatLng(
                                            document.get("latitude") as Double,
                                            document.get("longitude") as Double
                                        )
                                        // Set InfoWindow according to pin type with title and snippet details (for all pins this is done)
                                    ).draggable(
                                        false
                                    ).flat(false).snippet(document.get("message").toString())
                                )

                                when {
                                    // display 'cp' type pins as green
                                    document.get("type").toString() == "cp" -> {
                                        typeMarker.setIcon(
                                            BitmapDescriptorFactory.defaultMarker(
                                                BitmapDescriptorFactory.HUE_GREEN
                                            )
                                        )
                                        typeMarker.title =
                                            ("CP Pin - Callsign " + document.get("CS"))
                                    }
                                    document.get("type").toString() == "generic" -> {
                                        typeMarker.setIcon(
                                            BitmapDescriptorFactory.defaultMarker(
                                                BitmapDescriptorFactory.HUE_ORANGE
                                            )
                                        )
                                        typeMarker.title =
                                            ("Generic Pin - Callsign " + document.get("CS"))
                                    }
                                    document.get("type").toString() == "bq" -> {
                                        typeMarker.setIcon(
                                            BitmapDescriptorFactory.defaultMarker(
                                                BitmapDescriptorFactory.HUE_YELLOW
                                            )
                                        )
                                        typeMarker.title =
                                            ("BQ Pin - Callsign " + document.get("CS"))
                                    }
                                    document.get("type").toString() == "goto" -> {
                                        typeMarker.setIcon(
                                            BitmapDescriptorFactory.defaultMarker(
                                                BitmapDescriptorFactory.HUE_ROSE
                                            )
                                        )
                                        typeMarker.title =
                                            ("GOTO Pin - Callsign " + document.get("CS"))
                                    }
                                    document.get("type").toString() == "op" -> {
                                        typeMarker.setIcon(
                                            BitmapDescriptorFactory.defaultMarker(
                                                BitmapDescriptorFactory.HUE_AZURE
                                            )
                                        )
                                        typeMarker.title =
                                            ("OP Pin - Callsign " + document.get("CS"))
                                    }
                                    else -> {
                                        typeMarker.title =
                                            ("Unspecified Type Pin - Callsign " + document.get("CS"))
                                    }
                                }
                            }
                        }
                    }
            }
            val eventLatLng = LatLng(eventLat.toDouble(), eventLng.toDouble())

            val circleOptions =
                CircleOptions().center(eventLatLng).radius(200.0)
                    .strokeColor(Color.GREEN)
            mMap!!.addCircle(circleOptions)
            mMap!!.animateCamera(
                CameraUpdateFactory.newLatLngZoom(
                    eventLatLng,
                    16f
                )
            )
        }
    }

    override fun onPause() {
        super.onPause()

        //stop location updates when Activity is no longer active
        if (mFusedLocationClient != null) {
            mFusedLocationClient!!.removeLocationUpdates(mLocationCallback)
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        // set mMap to the current map
        mMap = googleMap

        // set map type to HYBRID
        mMap!!.mapType = GoogleMap.MAP_TYPE_HYBRID

        // get event coordinates
        val eventLatLng = LatLng(eventLat.toDouble(), eventLng.toDouble())

        moveToEventBtn.setOnClickListener {
            val circleOptions =
                CircleOptions().center(eventLatLng).radius(200.0).strokeColor(Color.GREEN)
            mMap!!.addCircle(circleOptions)
            mMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(eventLatLng, 16f))
        }

        // Set soldier name variable to the current users name for callsign in firestore
        db.collection("soldiers").document(userID).get().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                soldierName = task.result!!["name"].toString()
            }
        }

        db.collection("requests").get().addOnCompleteListener { taskRequests ->
            if (taskRequests.isSuccessful) {
                for (document in taskRequests.result!!) {
                    requests = document["requests"] as MutableList<String>
                }
            }
        }

        // set info click listener here
        googleMap.setOnInfoWindowClickListener(this@MapsActivity)

        // add current pins for the event on the map according to type
        // search map_request collection with whereEqualTo query search for more efficient search
        db.collection("map_request").whereEqualTo("eventId", eventID).get()
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    for (document in task.result!!) {
                        val typeMarker = mMap!!.addMarker(
                            MarkerOptions().position(
                                LatLng(
                                    document.get("latitude") as Double,
                                    document.get("longitude") as Double
                                )
                                // Set InfoWindow according to pin type with title and snippet details (for all pins this is done)
                            ).draggable(
                                false
                            ).flat(false).snippet(document.get("message").toString())
                        )

                        when {
                            // display 'cp' type pins as green
                            document.get("type").toString() == "cp" -> {
                                typeMarker.setIcon(
                                    BitmapDescriptorFactory.defaultMarker(
                                        BitmapDescriptorFactory.HUE_GREEN
                                    )
                                )
                                typeMarker.title = ("CP Pin - Callsign " + document.get("CS"))
                            }
                            document.get("type").toString() == "generic" -> {
                                typeMarker.setIcon(
                                    BitmapDescriptorFactory.defaultMarker(
                                        BitmapDescriptorFactory.HUE_ORANGE
                                    )
                                )
                                typeMarker.title = ("Generic Pin - Callsign " + document.get("CS"))
                            }
                            document.get("type").toString() == "bq" -> {
                                typeMarker.setIcon(
                                    BitmapDescriptorFactory.defaultMarker(
                                        BitmapDescriptorFactory.HUE_YELLOW
                                    )
                                )
                                typeMarker.title = ("BQ Pin - Callsign " + document.get("CS"))
                            }
                            document.get("type").toString() == "goto" -> {
                                typeMarker.setIcon(
                                    BitmapDescriptorFactory.defaultMarker(
                                        BitmapDescriptorFactory.HUE_ROSE
                                    )
                                )
                                typeMarker.title = ("GOTO Pin - Callsign " + document.get("CS"))
                            }
                            document.get("type").toString() == "op" -> {
                                typeMarker.setIcon(
                                    BitmapDescriptorFactory.defaultMarker(
                                        BitmapDescriptorFactory.HUE_AZURE
                                    )
                                )
                                typeMarker.title = ("OP Pin - Callsign " + document.get("CS"))
                            }

                            else -> {
                                // any other types of pins display with no colour (for now)
                                typeMarker.title =
                                    ("Unspecified Type Pin - Callsign " + document.get("CS"))
                            }
                        }


                    }
                }
            }

        // this section asynchronously 'listens' for changes in map_request documents while the activity is running.
        db.collection("map_request").whereEqualTo("eventId", eventID)
            .addSnapshotListener { snapshots, e ->
                if (e != null) {
                    Log.e(ERROR, "listen:error", e)
                    return@addSnapshotListener
                }

                for (dc in snapshots!!.documentChanges) {
                    // if a document is added, add the pin
                    if (dc.type == DocumentChange.Type.ADDED) {
                        Log.d(TAG, "Added Document ${dc.document.data}")
                        if (dc.document.data.get("eventId").toString() == eventID) {
                            val typeMarker = mMap!!.addMarker(
                                MarkerOptions().position(
                                    LatLng(
                                        dc.document.data.get("latitude") as Double,
                                        dc.document.data.get("longitude") as Double
                                    )
                                ).draggable(
                                    false
                                ).icon(
                                    BitmapDescriptorFactory.defaultMarker(
                                        BitmapDescriptorFactory.HUE_GREEN
                                    )
                                ).flat(false).snippet(dc.document.get("message").toString())
                            )

                            when {
                                // display 'cp' type pins as green
                                dc.document.get("type").toString() == "cp" -> {
                                    typeMarker.setIcon(
                                        BitmapDescriptorFactory.defaultMarker(
                                            BitmapDescriptorFactory.HUE_GREEN
                                        )
                                    )
                                    typeMarker.title =
                                        ("CP Pin - Callsign " + dc.document.get("CS"))
                                }
                                dc.document.get("type").toString() == "generic" -> {
                                    typeMarker.setIcon(
                                        BitmapDescriptorFactory.defaultMarker(
                                            BitmapDescriptorFactory.HUE_ORANGE
                                        )
                                    )
                                    typeMarker.title =
                                        ("Generic Pin - Callsign " + dc.document.get("CS"))
                                }
                                dc.document.get("type").toString() == "bq" -> {
                                    typeMarker.setIcon(
                                        BitmapDescriptorFactory.defaultMarker(
                                            BitmapDescriptorFactory.HUE_YELLOW
                                        )
                                    )
                                    typeMarker.title =
                                        ("BQ Pin - Callsign " + dc.document.get("CS"))
                                }
                                dc.document.get("type").toString() == "goto" -> {
                                    typeMarker.setIcon(
                                        BitmapDescriptorFactory.defaultMarker(
                                            BitmapDescriptorFactory.HUE_ROSE
                                        )
                                    )
                                    typeMarker.title =
                                        ("GOTO Pin - Callsign " + dc.document.get("CS"))
                                }
                                dc.document.get("type").toString() == "op" -> {
                                    typeMarker.setIcon(
                                        BitmapDescriptorFactory.defaultMarker(
                                            BitmapDescriptorFactory.HUE_AZURE
                                        )
                                    )
                                    typeMarker.title =
                                        ("OP Pin - Callsign " + dc.document.get("CS"))
                                }

                                else -> {
                                    // any other types of pins display with no colour (for now)
                                    typeMarker.title =
                                        ("Unspecified Type Pin - Callsign " + dc.document.get("CS"))
                                }
                            }
                        }
                    }

                    // if a field in a document is changed/removed, clear map and re-add the event pins
                    if (dc.type == DocumentChange.Type.MODIFIED) {
                        mMap!!.clear()
                        db.collection("map_request").whereEqualTo("eventId", eventID).get()
                            .addOnCompleteListener { task ->
                                if (task.isSuccessful) {
                                    for (document in task.result!!) {
                                        val typeMarker = mMap!!.addMarker(
                                            MarkerOptions().position(
                                                LatLng(
                                                    document.get("latitude") as Double,
                                                    document.get("longitude") as Double
                                                )
                                                // Set InfoWindow according to pin type with title and snippet details (for all pins this is done)
                                            ).draggable(
                                                false
                                            ).flat(false).snippet(document.get("message").toString())
                                        )

                                        when {
                                            // display 'cp' type pins as green
                                            document.get("type").toString() == "cp" -> {
                                                typeMarker.setIcon(
                                                    BitmapDescriptorFactory.defaultMarker(
                                                        BitmapDescriptorFactory.HUE_GREEN
                                                    )
                                                )
                                                typeMarker.title =
                                                    ("CP Pin - Callsign " + document.get("CS"))
                                            }
                                            document.get("type").toString() == "generic" -> {
                                                typeMarker.setIcon(
                                                    BitmapDescriptorFactory.defaultMarker(
                                                        BitmapDescriptorFactory.HUE_ORANGE
                                                    )
                                                )
                                                typeMarker.title =
                                                    ("Generic Pin - Callsign " + document.get("CS"))
                                            }
                                            document.get("type").toString() == "bq" -> {
                                                typeMarker.setIcon(
                                                    BitmapDescriptorFactory.defaultMarker(
                                                        BitmapDescriptorFactory.HUE_YELLOW
                                                    )
                                                )
                                                typeMarker.title =
                                                    ("BQ Pin - Callsign " + document.get("CS"))
                                            }
                                            document.get("type").toString() == "goto" -> {
                                                typeMarker.setIcon(
                                                    BitmapDescriptorFactory.defaultMarker(
                                                        BitmapDescriptorFactory.HUE_ROSE
                                                    )
                                                )
                                                typeMarker.title =
                                                    ("GOTO Pin - Callsign " + document.get("CS"))
                                            }
                                            document.get("type").toString() == "op" -> {
                                                typeMarker.setIcon(
                                                    BitmapDescriptorFactory.defaultMarker(
                                                        BitmapDescriptorFactory.HUE_AZURE
                                                    )
                                                )
                                                typeMarker.title =
                                                    ("OP Pin - Callsign " + document.get("CS"))
                                            }

                                            else -> {
                                                // any other types of pins display with no colour (for now)
                                                typeMarker.title =
                                                    ("Unspecified Type Pin - Callsign " + document.get(
                                                        "CS"
                                                    ))
                                            }
                                        }
                                    }
                                }
                            }
                    }

                    // if document is removed from collection, clear map and re-add pins
                    if (dc.type == DocumentChange.Type.REMOVED) {
                        mMap!!.clear()
                        db.collection("map_request").whereEqualTo("eventId", eventID).get()
                            .addOnCompleteListener() { task ->
                                if (task.isSuccessful) {
                                    for (document in task.result!!) {
                                        val typeMarker = mMap!!.addMarker(
                                            MarkerOptions().position(
                                                LatLng(
                                                    document.get("latitude") as Double,
                                                    document.get("longitude") as Double
                                                )
                                                // Set InfoWindow according to pin type with title and snippet details (for all pins this is done)
                                            ).draggable(
                                                false
                                            ).flat(false).snippet(document.get("message").toString())
                                        )

                                        when {
                                            // display 'cp' type pins as green
                                            document.get("type").toString() == "cp" -> {
                                                typeMarker.setIcon(
                                                    BitmapDescriptorFactory.defaultMarker(
                                                        BitmapDescriptorFactory.HUE_GREEN
                                                    )
                                                )
                                                // any other types of pins display with no colour (for now)
                                                typeMarker.title = ("CP Type Pin - Callsign " + document.get("CS"))
                                            }
                                            document.get("type").toString() == "generic" -> {
                                                typeMarker.setIcon(
                                                    BitmapDescriptorFactory.defaultMarker(
                                                        BitmapDescriptorFactory.HUE_ORANGE
                                                    )
                                                )
                                                typeMarker.title = ("Generic Type Pin - Callsign " + document.get("CS"))
                                            }
                                            document.get("type").toString() == "bq" -> {
                                                typeMarker.setIcon(
                                                    BitmapDescriptorFactory.defaultMarker(
                                                        BitmapDescriptorFactory.HUE_YELLOW
                                                    )
                                                )
                                                typeMarker.title = ("BQ Type Pin - Callsign " + document.get("CS"))
                                            }
                                            document.get("type").toString() == "goto" -> {
                                                typeMarker.setIcon(
                                                    BitmapDescriptorFactory.defaultMarker(
                                                        BitmapDescriptorFactory.HUE_ROSE
                                                    )
                                                )
                                                typeMarker.title = ("GOTO Type Pin - Callsign " + document.get("CS"))
                                            }
                                            document.get("type").toString() == "op" -> {
                                                typeMarker.setIcon(
                                                    BitmapDescriptorFactory.defaultMarker(
                                                        BitmapDescriptorFactory.HUE_AZURE
                                                    )
                                                )
                                                typeMarker.title = ("OP Type Pin - Callsign " + document.get("CS"))
                                            }

                                            else -> {
                                                // any other types of pins display with no colour (for now)
                                                typeMarker.title =
                                                    ("Unspecified Type Pin - Callsign " + document.get(
                                                        "CS"
                                                    ))
                                            }
                                        }
                                    }
                                }
                            }
                    }
                }
            }

        // on map click listener to add pins with basic Info Window
        mMap!!.setOnMapClickListener { point ->
            //add marker to where you clicked
            val marker = MarkerOptions().position(LatLng(point.latitude, point.longitude))
                .title("Request Pin - Callsign: " + soldierName)
                .draggable(true)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE))
                .flat(false).snippet("Click for more details")

            // add the marker to map
            mMap!!.addMarker(marker)

            val mapsMarker =
                MapMarker(
                    marker.position.latitude,
                    marker.position.longitude
                )

            allPoints!!.add(mapsMarker)

            // move map to the markers location.
            mMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(marker.position, 16f))
        }

        // set on marker click listener to move map to marker clicked
        googleMap.setOnMarkerClickListener { marker ->
            Log.d(TAG, "Marker clicked")
            mMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(marker.position, 16f))
            if (marker.isInfoWindowShown) {
                marker.hideInfoWindow()
            } else {
                marker.showInfoWindow()
            }
            true
        }

        // Request Location variable.
        mLocationRequest = LocationRequest()
        // Check current location every 1000000 milliseconds
        mLocationRequest.fastestInterval = 1000000
        // sets the request priority to a balanced mode for semi-good accuracy, other modes will focus on battery life, or better accuracy.
        mLocationRequest.priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY

        // checks if the minimum build version is high enough to check for location permission, or just set location.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // if location permission is granted
            if (ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) === PackageManager.PERMISSION_GRANTED
            ) {
                //Location Permission already granted
                mFusedLocationClient!!.requestLocationUpdates(
                    mLocationRequest,
                    mLocationCallback,
                    Looper.myLooper()
                )
                mMap!!.isMyLocationEnabled = true
            } else {
                //Request Location Permission
                checkLocationPermission()
            }
        } else {
            mFusedLocationClient!!.requestLocationUpdates(
                mLocationRequest,
                mLocationCallback,
                Looper.myLooper()
            )
            mMap!!.isMyLocationEnabled = true
        }
    }

    internal var mLocationCallback: LocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            // set all the location results into a location list
            val locationList = locationResult.locations
            if (locationList.size > 0) {
                //The last location in the list is the newest, set it as location
                val location = locationList[locationList.size - 1]
                Log.d(TAG, "Location: " + location.latitude + " " + location.longitude)
                mLastLocation = location
                if (mCurrLocationMarker != null) {
                    mCurrLocationMarker!!.remove()
                }

                //Place current location marker
                val eventLatLng =
                    LatLng(mLastLocation.latitude, mLastLocation.longitude)

                //move map camera to current location
                mMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(eventLatLng, 12f))
            }
        }
    }

    private val MY_PERMISSIONS_REQUEST_LOCATION = 99

    private fun checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) !== PackageManager.PERMISSION_GRANTED
        ) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                )
            ) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                AlertDialog.Builder(this)
                    .setTitle("Location Permission Needed")
                    .setMessage("This app needs the Location permission, please accept to use location functionality")
                    .setPositiveButton("OK") { dialogInterface, i ->
                        //Prompt the user once explanation has been shown
                        ActivityCompat.requestPermissions(
                            this@MapsActivity,
                            arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                            MY_PERMISSIONS_REQUEST_LOCATION
                        )
                    }
                    .create()
                    .show()
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    MY_PERMISSIONS_REQUEST_LOCATION
                )
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            MY_PERMISSIONS_REQUEST_LOCATION -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay!
                    if (ContextCompat.checkSelfPermission(
                            this,
                            Manifest.permission.ACCESS_FINE_LOCATION
                        ) === PackageManager.PERMISSION_GRANTED
                    ) {
                        mFusedLocationClient!!.requestLocationUpdates(
                            mLocationRequest,
                            mLocationCallback,
                            Looper.myLooper()
                        )
                        mMap!!.isMyLocationEnabled = true
                    }
                } else {
                    // permission denied, boo!
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show()
                }
                return
            }
        }
    }

    fun locationSubmit(view: View) {
        // if there are GeoPoints, or LatLngs on the map, create the document fields using the mapRequest variable.
        if (allPoints != null) {
            for (i in allPoints!!.indices) {
                mapRequest["latitude"] = allPoints!![i].latitude
                mapRequest["longitude"] = allPoints!![i].longitude
                mapRequest["type"] = type
                mapRequest["userID"] = userID
                mapRequest["CS"] = soldierName
                mapRequest["requests"] = requestsList
                mapRequest["message"] = "Request Pin for $soldierName"

                // add the mapRequest as a document in map_requests collection
                db.collection("map_request")
                    .add(mapRequest)
                    .addOnSuccessListener { documentReference ->
                        Log.d(
                            SUCCESS,
                            "DocumentSnapshot added with ID: " + documentReference.id
                        )
                        // on success, set the eventId to the document just added
                        mapRequest["eventId"] = eventID
                        db.collection("map_request").document(documentReference.id)
                            .set(mapRequest, SetOptions.merge())
                    }
                    .addOnFailureListener { e ->
                        Log.w(FAIL, "Error adding document", e)
                    }
                mapRequest.clear()
            }

            // add userID and eventID into the intent and finish.
            val intent = Intent()
            val event = Bundle()
            event.putString("userID", userID)
            event.putString("eventID", eventID)
            intent.putExtras(event)
            finish()
        } else { // if no points were added to the map, go back with intent and skip document creation
            val toast = Toast.makeText(
                applicationContext,
                "Did not create Resource Document",
                Toast.LENGTH_LONG
            )
            toast.show()
            val intent = Intent()

            val event = Bundle()
            event.putString("userID", userID)
            event.putString("eventID", eventID)
            intent.putExtras(event)
            finish()
        }
    }

    // This method will be invoked when user clicks android device Back button at bottom.
    override fun onBackPressed() {
        val toast = Toast.makeText(
            applicationContext,
            "Did not create Resource Document",
            Toast.LENGTH_LONG
        )
        toast.show()
        val intent = Intent()

        val event = Bundle()
        //lat = mLastLocation.latitude.toString()
        //lng = mLastLocation.longitude.toString()
        event.putString("userID", userID)
        event.putString("eventID", eventID)
        intent.putExtras(event)
        finish()
    }


    // allow a request to be made on InfoWindow click and add the request to the pins document.
    override fun onInfoWindowClick(marker: Marker?) {
        //Get the first word from the title to get the type without calling firestore.
        val markerTitleType = marker?.title.toString()
        val type = markerTitleType.split(" ")

        val firstWord = type[0]

        // if the pin type is 'request', display the request dialog for soldiers.
        if (firstWord == "Request") {

            val builder = AlertDialog.Builder(this@MapsActivity)

            //requested items array for firestore update
            var requestedItems: ArrayList<String> = ArrayList()

            builder.setTitle("Make a Request")
                .setMultiChoiceItems(requests.toTypedArray(), null,
                    DialogInterface.OnMultiChoiceClickListener { dialog, which, isChecked ->
                        if (isChecked) {
                            // If the user checked the item, add it to the selected items
                            requestedItems.add(requests[which])
                            Log.d(TAG, requestedItems.toString())
                        }
                    })
                // Set the action buttons
                .setPositiveButton(
                    "Confirm",
                    DialogInterface.OnClickListener { dialog, id ->
                        // User clicked Confirm, so save the selectedItems results
                        requestsList = requestedItems

                        try {
                            db.collection("map_request").whereEqualTo("eventId", eventID).get()
                                .addOnCompleteListener() { task ->
                                    if (task.isSuccessful) {
                                        for (document in task.result!!) {
                                            if (document["latitude"] == marker?.position?.latitude
                                                && document["longitude"] == marker?.position?.longitude
                                            ) {
                                                mapRequest["requests"] = requestedItems

                                                db.collection("map_request").document(document.id)
                                                    .set(mapRequest, SetOptions.merge())

                                                Toast.makeText(
                                                    this@MapsActivity,
                                                    "Request Confirmed",
                                                    Toast.LENGTH_SHORT
                                                ).show()
                                            }
                                        }
                                    }
                                }
                        } catch (e: Exception) {
                        }
                    })
                .setNegativeButton(
                    "Cancel",
                    DialogInterface.OnClickListener { dialog, id ->
                        // what ever you want to do with Cancel option.
                        Toast.makeText(
                            this@MapsActivity,
                            "Request Cancelled",
                            Toast.LENGTH_SHORT
                        ).show()
                    })
            builder.show()
        }
    }
}

