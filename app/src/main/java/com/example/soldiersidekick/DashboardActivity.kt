package com.example.soldiersidekick

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_login.btnLogin
import kotlinx.android.synthetic.main.activity_main.*

class DashboardActivity: AppCompatActivity() {

    private var userID = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_dashboard)

        val intent = getIntent()

        // get userID with intent
        userID = intent.getStringExtra("userID")

    }

    // request attendance to event button
    fun onEventRequestClick(view: View) {
        val intent = Intent(this, EventListActivity::class.java)
        intent.putExtra("userID", userID)
        startActivity(intent)
    }

    // chatbot assistant button
    fun onAssistantClick(view: View) {
        val intent = Intent(this, VirtualAssistant::class.java)
        startActivity(intent)
    }

    // event list, event coordination and exercises button
    fun onEventListClick(view: View) {
        val intent = Intent(this, EventRequestActivity::class.java)
        intent.putExtra("userID", userID)
        startActivity(intent)
    }

    // edit profile button
    fun onEditClick(view: View) {
        val intent = Intent(this, EditActivity::class.java)
        intent.putExtra("userID", userID)
        startActivity(intent)
    }
}