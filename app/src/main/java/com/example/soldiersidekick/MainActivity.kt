package com.example.soldiersidekick

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.telephony.PhoneNumberUtils
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.example.soldiersidekick.Models.Soldier
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_main.*
import java.util.regex.Matcher
import java.util.regex.Pattern


class MainActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {

    private lateinit var auth: FirebaseAuth
    var rank = ""
    var rankList: ArrayList<String> = ArrayList()

    var qualificationsList: ArrayList<String> = ArrayList()
    lateinit var qualList: Array<String>

    var driverQualificationsList: ArrayList<String> = ArrayList()
    lateinit var driverQualList: Array<String>

    // general tag for debugging/error logging
    private val EMPTY = "Empty Select"

    private val emailREGEX = Pattern.compile("^[A-Za-z](.*)([@])(.+)(\\.)(.+)(\\.)(.+)")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        auth = FirebaseAuth.getInstance()

        val db = FirebaseFirestore.getInstance()

        val qualBtn: Button = findViewById(R.id.qualifications_btn)
        val driverQualBtn: Button = findViewById(R.id.driver_qual_btn)

        db.collection("ranks").get().addOnCompleteListener { taskRank ->
            if (taskRank.isSuccessful) {
                for (document in taskRank.result!!) {
                    // Create an ArrayAdapter using the 'rank' string array from firebase and a default spinner layout
                    rankList = document.get("ranks") as ArrayList<String>

                    val rankAdapter = ArrayAdapter(
                        this@MainActivity,
                        android.R.layout.simple_spinner_dropdown_item,
                        rankList
                    )
                    rankAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

                    rankSpinner.adapter = rankAdapter

                    rankSpinner.onItemSelectedListener = this
                }
            }
        }

        qualBtn.setOnClickListener {
            // get qualification query from firebase
            db.collection("qualifications").get().addOnCompleteListener { taskQual ->
                if (taskQual.isSuccessful) {
                    for (document in taskQual.result!!) {
                        // get qualifications and make a typed list that the alert dialog can recognize.
                        qualificationsList = document.get("qualifications") as ArrayList<String>
                        qualList = qualificationsList.toTypedArray()

                        val builder = AlertDialog.Builder(this@MainActivity)

                        var qualificationSelected: ArrayList<String> = ArrayList()

                        // make dialog box with the qualification list
                        builder.setTitle("Choose your Qualifications")
                            .setMultiChoiceItems(qualList, null,
                                DialogInterface.OnMultiChoiceClickListener { dialog, which, isChecked ->
                                    if (isChecked) {
                                        // If the user checked the item, add it to the selected items
                                        qualificationSelected.add(qualList[which])
                                    }
                                })
                            // Set the action buttons
                            .setPositiveButton(
                                "Confirm",
                                DialogInterface.OnClickListener { dialog, id ->
                                    // User clicked Confirm, so save the selectedItems results somewhere
                                    // or return them to the component that opened the dialog
                                    try {
                                        qualificationsList = qualificationSelected
                                    } catch (e: Exception) {
                                    }
                                })

                        builder.show()
                    }
                }
            }
        }

        driverQualBtn.setOnClickListener {
            // get driver qualifications query from firebase
            db.collection("driverQualifications").get().addOnCompleteListener { taskDriverQual ->
                if (taskDriverQual.isSuccessful) {
                    for (document in taskDriverQual.result!!) {
                        driverQualificationsList =
                            document.get("driverQualifications") as ArrayList<String>
                        driverQualList = driverQualificationsList.toTypedArray()

                        val builder = AlertDialog.Builder(this@MainActivity)

                        var driverQualSelected: ArrayList<String> = ArrayList()

                        builder.setTitle("Choose your Driver Qualifications")
                            .setMultiChoiceItems(driverQualList, null,
                                DialogInterface.OnMultiChoiceClickListener { dialog, which, isChecked ->
                                    if (isChecked) {
                                        // If the user checked the item, add it to the selected items
                                        driverQualSelected.add(driverQualList[which])
                                    }
                                })
                            // Set the action buttons
                            .setPositiveButton(
                                "Confirm",
                                DialogInterface.OnClickListener { dialog, id ->
                                    // User clicked Confirm, so save the selectedItems results somewhere
                                    // or return them to the component that opened the dialog
                                    try {
                                        driverQualificationsList = driverQualSelected
                                    } catch (e: Exception) {
                                    }
                                })

                        builder.show()
                    }
                }
            }
        }

        btnRegister.setOnClickListener {

            // check if all fields are filled
            if (edtFirstName.text.isNotEmpty() && edtLastName.text.isNotEmpty()
                && edtEmail.text.isNotEmpty() && edtPhone.text.isNotEmpty() && edtPassword.text.isNotEmpty()
            ) {
                var emailMatch: Matcher = emailREGEX.matcher(edtEmail.text.toString())
                // if email matches its regex statement and domain is exactly "forces.gc.ca"
                if (emailMatch.matches() && edtEmail.text.toString().substringAfter("@") == "forces.gc.ca") {
                    // if phone number is a global phone number and is 10 digits
                    if (PhoneNumberUtils.isGlobalPhoneNumber(edtPhone.text.toString()) && edtPhone.text.length == 10) {
                        // if password length is greater than 8 and the password matches the confirm password field.
                        if (edtPassword.text.length >= 8) {
                            if (edtPassword.text.toString() != edtConfirmPassword.text.toString()) {
                                Toast.makeText(
                                    this,
                                    "Password does not match confirm password",
                                    Toast.LENGTH_LONG
                                ).show()
                            } else {
                                val email = edtEmail.text.toString()
                                val password = edtPassword.text.toString()

                                // Fire base auth to create a user with email and password
                                auth.createUserWithEmailAndPassword(email, password)
                                    .addOnCompleteListener(this) { task ->
                                        if (task.isSuccessful) {

                                            // Sign in success, update UI with the signed-in user's information
                                            createUserInFirebase(task.result!!.user!!.uid)

                                        } else {
                                            // If sign in fails, display a message to the user.
                                            Log.w(
                                                "Main",
                                                "createUserWithEmail:failure",
                                                task.exception
                                            )
                                            Toast.makeText(
                                                baseContext, "Authentication failed.",
                                                Toast.LENGTH_SHORT
                                            ).show()
                                        }

                                    }
                            }
                        } else {
                            edtPassword.error = "Please make the password 8 characters or more"
                        }
                    } else {
                        edtPhone.error = "Input a 10 digit phone number please."
                    }
                } else {
                    edtEmail.error = "Email must have a domain of 'forces.gc.ca'"
                }
            } else {
                Toast.makeText(
                    this,
                    "Please fill in all of the fields before registering",
                    Toast.LENGTH_LONG
                ).show()
            }
        }

        btnLogin.setOnClickListener {
            // launch the login activity
            val intent = Intent(this, LoginActivity::class.java)

            startActivity(intent)
        }
    }

    fun createUserInFirebase(userid: String) {

        //Get the values from the UI Controls
        val firstName = edtFirstName.text.toString()
        val lastName = edtLastName.text.toString()
        val phone = edtPhone.text.toString()
        val email = edtEmail.text.toString()

        //Create mutable lists for the qualifications and four o fours for firebase
        var qualifications: MutableList<String> = qualificationsList

        var fourZeroFours: MutableList<String> = driverQualificationsList

        //Add properties such as attending, not attending, confirmed attending, and courses to have better insight over who is attending which events.
        val attending = mutableListOf<String>()
        val notAttending = mutableListOf<String>()
        val confirmedAttending = mutableListOf<String>()
        val course = mutableListOf<String>()

        val soldier = Soldier(
            firstName,
            lastName,
            rank,
            email,
            phone,
            qualifications,
            fourZeroFours,
            false
        )

        //create firestore document layout using hashmap
        val soldierData = hashMapOf(
            "firstName" to soldier.firstName,
            "lastName" to soldier.lastName,
            "name" to "$firstName $lastName",
            "rank" to soldier.rank,
            "email" to soldier.email,
            "phoneNumber" to soldier.phoneNumber,
            "qualifications" to soldier.qualifications,
            "driverQualifications" to soldier.driverQual,
            "attendingEvents" to attending,
            "notAttending" to notAttending,
            "confirmedAttending" to confirmedAttending,
            "courses" to course,
            "admin" to soldier.admin
        )

        //Access the firestore
        val db = FirebaseFirestore.getInstance()

        db.collection("soldiers").document(userid).set(soldierData)

        // launch the login activity so the user can log in.
        val intent = Intent(this, LoginActivity::class.java)

        intent.putExtra("userID", userid)

        startActivity(intent)
    }

    // override methods for Spinner class
    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
        // An item was selected.
        rank = p0?.getItemAtPosition(p2) as String
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
        Log.d(EMPTY, "Nothing selected in dropdown")
    }
}
