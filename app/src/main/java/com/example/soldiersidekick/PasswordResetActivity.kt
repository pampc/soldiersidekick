package com.example.soldiersidekick

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.EmailAuthProvider
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_password_reset.*

class PasswordResetActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    private val TAG = "PasswordResetActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        auth = FirebaseAuth.getInstance()
        setContentView(R.layout.activity_password_reset)
    }

    fun confirmReset(view: View) {
        changePassword()
    }

    private fun changePassword() {

        if (et_confirm_password.text.isNotEmpty() && et_current_password.text.isNotEmpty()
            && et_new_password.text.isNotEmpty()
        ) {
            if (et_new_password.text.toString() == et_confirm_password.text.toString()) {

                val user = auth.currentUser

                if (user != null && user.email != null) {
                    // Get auth credentials from the user for re-authentication. The example below shows
                    // email and password credentials but there are multiple possible providers,
                    // such as GoogleAuthProvider or FacebookAuthProvider.
                    val credential = EmailAuthProvider
                        .getCredential(user.email!!, et_current_password.text.toString())

                    // Prompt the user to re-provide their sign-in credentials
                    user?.reauthenticate(credential)
                        ?.addOnCompleteListener {
                            if (it.isSuccessful) {
                                Log.d(TAG, "User re-authenticated.")
                                val newPassword = et_new_password.text.toString()

                                user?.updatePassword(newPassword)
                                    ?.addOnCompleteListener { task ->
                                        if (task.isSuccessful) {
                                            Toast.makeText(this, "Password Changed Successfully!", Toast.LENGTH_SHORT).show()

                                            auth.signOut()

                                            startActivity(Intent(this, LoginActivity::class.java))
                                        }
                                    }
                            }
                            else {
                                Log.d(TAG, "Re-authentication Failed")
                                Toast.makeText(this, "Re-authentication Failed", Toast.LENGTH_SHORT).show()
                            }
                        }
                }
                else {
                    startActivity(Intent(this, LoginActivity::class.java))
                }
            }
            else {
                Toast.makeText(this, "Password Mismatch", Toast.LENGTH_SHORT).show()
            }
        }
        else {
            Toast.makeText(this, "Please fill in the fields", Toast.LENGTH_SHORT).show()
        }
    }
}
