package com.example.soldiersidekick.Models

class MapMarker(var latitude: Double, var longitude: Double) {

    fun MapMarker(latitude: Double, longitude: Double) {
        this.latitude = latitude
        this.longitude = longitude
    }

    fun getPointLatitude(): Double {
        return latitude
    }

    fun getPointLongitude(): Double {
        return longitude
    }
}