package com.example.soldiersidekick

import android.R.attr
import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.text.InputType
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.EmailAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_email_reset.*
import java.util.regex.Matcher
import java.util.regex.Pattern


class EmailResetActivity : AppCompatActivity() {

    private val TAG = "EmailResetActivity"
    private val SUCCESS = "Success"
    private val FAIL = "Fail"

    private var Pass = ""
    private val auth = FirebaseAuth.getInstance()
    lateinit var db: FirebaseFirestore
    lateinit var userID: String

    private val emailREGEX = Pattern.compile("^[A-Za-z](.*)([@])(.+)(\\.)(.+)(\\.)(.+)")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_email_reset)

        val intent = intent
        val extras = intent.extras

        userID = extras?.getString("userID").toString()
    }

    @SuppressLint("InflateParams")
    fun confirmReset(view: View) {

        db = FirebaseFirestore.getInstance()

        var snMatch: Matcher = emailREGEX.matcher(et_new_email.text.toString())

        if (snMatch.matches() && et_new_email.text.toString().substringAfter("@") == "forces.gc.ca") {
            if (et_new_email.text.toString() == et_confirm_email.text.toString()) {

                val user = auth.currentUser

                if (user != null && user.email != null) {

                    val context = this
                    val builder = AlertDialog.Builder(context)
                    builder.setTitle("Password Validation")
                    builder.setMessage("Please Input Password to verify User Account")

                    val password = EditText(this@EmailResetActivity)

                    password.inputType = InputType.TYPE_TEXT_VARIATION_PASSWORD

                    builder.setView(password)

                    // set up the ok button
                    builder.setPositiveButton("Confirm") { dialog, p1 ->
                        Pass = password.text.toString()
                        var isValid = true
                        if (Pass.isBlank() || Pass.length < 8) {
                            Toast.makeText(
                                this,
                                "Password was incorrect, please try again",
                                Toast.LENGTH_SHORT
                            ).show()
                            isValid = false
                        }

                        if (isValid && Pass.length >= 8) {
                            dialog.dismiss()

                            // Get auth credentials from the user for re-authentication. The example below shows
                            // email and password credentials but there are multiple possible providers,
                            // such as GoogleAuthProvider or FacebookAuthProvider.
                            val credential = EmailAuthProvider
                                .getCredential(et_current_email.text.toString(), Pass)

                            // Prompt the user to re-provide their sign-in credentials
                            user?.reauthenticate(credential)
                                ?.addOnCompleteListener {
                                    if (it.isSuccessful) {
                                        Log.d(TAG, "User re-authenticated.")

                                        user?.updateEmail(et_new_email.text.toString())
                                            ?.addOnCompleteListener { task ->
                                                if (task.isSuccessful) {
                                                    Toast.makeText(
                                                        this,
                                                        "Email Changed Successfully!",
                                                        Toast.LENGTH_SHORT
                                                    ).show()

                                                    db.collection("soldiers").document(userID).update(
                                                        mapOf(
                                                            "email" to et_new_email.text.toString()
                                                        )
                                                    ).addOnSuccessListener {
                                                        Log.d(
                                                            SUCCESS,
                                                            "DocumentSnapshot successfully written!"
                                                        )
                                                    }.addOnFailureListener { e ->
                                                        Log.w(
                                                            FAIL,
                                                            "Error writing document",
                                                            e
                                                        )
                                                    }
                                                    auth.signOut()

                                                    startActivity(
                                                        Intent(
                                                            this,
                                                            LoginActivity::class.java
                                                        )
                                                    )
                                                }
                                            }
                                    } else {
                                        Log.d(TAG, "Re-authentication Failed")
                                        Toast.makeText(
                                            this,
                                            "Re-authentication Failed",
                                            Toast.LENGTH_SHORT
                                        ).show()
                                    }
                                }
                        }
                        else {
                            Toast.makeText(
                                this,
                                "Password was incorrect, please try again",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }

                    builder.setNegativeButton(android.R.string.cancel) { dialog, p1 ->
                        dialog.cancel()
                    }

                    builder.show()
                }
                else {
                    startActivity(Intent(this, LoginActivity::class.java))
                }
            }
            else {
                et_new_email.error = "Email Mismatch"
                et_confirm_email.error = "Email Mismatch"
            }
        }
        else {
            et_new_email.error = "Must have a domain of forces.gc.ca"
        }
    }
}
