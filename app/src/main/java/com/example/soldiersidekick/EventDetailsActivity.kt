package com.example.soldiersidekick

import android.content.Intent
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity

import kotlinx.android.synthetic.main.activity_event_details.*
import kotlinx.android.synthetic.main.fragment_event_details.*
import android.content.DialogInterface
import androidx.core.app.ComponentActivity
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import androidx.appcompat.app.AlertDialog


class EventDetailsActivity : AppCompatActivity() {

    // global variables for the event details to be filled with intent extras
    var userID = ""
    var eventID = ""
    var title = ""
    var instructor = ""
    var order = ""
    var startDate = ""
    var endDate = ""
    var desc = ""
    var eventLocation = ""
    var eventLat = ""
    var eventLng = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_event_details)

        val intent = intent
        val extras = intent.extras

        // fill variables with values from intent extras
        userID = extras?.getString("userID").toString()
        eventID = extras?.getString("eventID").toString()
        title = extras?.getString("title").toString()
        instructor = extras?.getString("instructor").toString()
        order = extras?.getString("orders").toString()
        startDate = extras?.getString("startDate").toString()
        endDate = extras?.getString("endDate").toString()
        desc = extras?.getString("desc").toString()
        eventLocation = extras?.getString("location").toString()
        eventLat = extras?.getString("eventLat").toString()
        eventLng = extras?.getString("eventLng").toString()

        // set the corresponding edittexts to the proper event variables.
        eventDetailTitle.text = title
        eventDetailOrders.text = "Orders: " + order
        eventDetailInstructor.text = "Instructor: " + instructor
        eventDetailStartTime.text = startDate + " - "
        eventDetailEndTime.text = endDate
        eventDetailDesc.text = "Event Description: " + desc
        eventDetailLocation.text = "Location: " + eventLocation

        fab.setOnClickListener {

            // when map button is clicked, go to map with the userID, eventID, and the event coordinates.
            val extras = Bundle()

            val intent = Intent(this@EventDetailsActivity, MapsActivity::class.java)

            extras.putString("userID",
                userID
            )
            extras.putString("eventID",
                eventID
            )
            extras.putString("eventLat",
                eventLat
            )
            extras.putString("eventLng",
                eventLng
            )

            intent.putExtras(extras)

            startActivity(intent)
        }
    }


}
