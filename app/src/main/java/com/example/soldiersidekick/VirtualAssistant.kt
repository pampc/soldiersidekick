package com.example.soldiersidekick

import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_virtual_assistant.*
import android.webkit.WebViewClient
import android.webkit.WebChromeClient

class VirtualAssistant : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_virtual_assistant)

        chatBot.setInitialScale(1)
        chatBot.webChromeClient = WebChromeClient()
        //chatBot.settings.allowFileAccess = true
        //chatBot.webViewClient = WebViewClient()
        chatBot.settings.javaScriptEnabled = true
        chatBot.settings.loadWithOverviewMode = true
        chatBot.settings.useWideViewPort = true

        val data_html =
            "<!DOCTYPE html><html> <head> <meta charset=\"UTF-8\"><meta name=\"viewport\" content=\"target-densitydpi=high-dpi\" /> <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"> <link rel=\"stylesheet\" media=\"screen and (-webkit-device-pixel-ratio:1.5)\" /></head> <body> <iframe\n" +
                    "    allow=\"microphone;\"\n" +
                    "    scrolling=\"yes\"\n" +
                    "    width=\"100%\"\n" +
                    "    height=\"600\"\n" +
                    "    src=\"https://console.dialogflow.com/api-client/demo/embedded/19d3b0da-c0d3-4aad-9945-c2e730ddb928\">\n" +
                    "</iframe> </body> </html> "

        //chatBot.loadDataWithBaseURL("http://bot.dialogflow.com", data_html, "text/html", "UTF-8", null)
        chatBot.loadData(data_html, "text/html", null);
    }
}
