package com.example.soldiersidekick

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.Query
import kotlinx.android.synthetic.main.activity_event_requests.*
import java.util.*
import kotlin.collections.ArrayList

class EventRequestActivity : AppCompatActivity() {

    // List of events to display using array adapter
    var eventList: ArrayList<String> = ArrayList()

    // userID variable for intent
    var userID = ""

    // array adapter used to display event documents in ListView
    lateinit var arrayAdapterFBEvents: ArrayAdapter<String>

    // used to hold the event document id's for easy access when checking firestore.
    var eventIDs = mutableListOf<String>()

    val c: Date = Calendar.getInstance().time

    // general tag for debugging/error logging
    private val TAG = "EventRequestActivity"
    private val SUCCESS = "Success"
    private val FAIL = "Fail"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_event_requests)

        val lstEvents = findViewById<ListView>(R.id.lstEvents)
        userID = intent.extras?.getString("userID").toString()

        // set not attending button to visible
        notAttendingBtn.visibility = View.VISIBLE

        //Access the firestore
        val db = FirebaseFirestore.getInstance()
        // get event documents and soldier documents
        db.collection("events").orderBy("createdDate", Query.Direction.DESCENDING).get().addOnCompleteListener() { taskEvent ->
            if (taskEvent.isSuccessful) {
                db.collection("soldiers").document(userID).get().addOnCompleteListener { taskSoldier ->
                    if (taskSoldier.isSuccessful) {
                        // make 'notAttending' a list to check which events a soldier has chosen to not attend beforehand.
                        var notAttending = taskSoldier.result!!["notAttending"] as List<String>
                        Log.d(TAG, "DOCUMENT CHECK SUCCESS")
                        for (document in taskEvent.result!!) {
                            // make 'isAttending' a list from event documents to see which userID's are attending certain events
                            var isAttending = document.get("attending") as List<String>

                            val startTime =
                                document.get("startDate") as com.google.firebase.Timestamp
                            val endTime =
                                document.get("endDate") as com.google.firebase.Timestamp

                            val startDate = startTime.toDate()
                            val endDate = endTime.toDate()

                            // if events cant be displayed, display a friendly toast and don't make list.
                            if (isAttending.contains(userID) || notAttending.contains(document.id) || document.get("draft") == true || c > startDate) {
                                Toast.makeText(
                                    this@EventRequestActivity,
                                    "Please choose available Events (if applicable)",
                                    Toast.LENGTH_LONG
                                ).show()

                            } else {
                                // event ids to add to soldier attending array in firebase
                                eventIDs.add(document.id)

                                var events =
                                    "\n$startDate - $endDate\n\n" + document.get(
                                        "title"
                                    ) + "\n" + "Instructor: " + document.get("instructor") + "\n" +
                                            "Location: " + document.get("location") + "\n" + "\n" +
                                            document.get("exerciseDescription") + "\n"

                                eventList.add(events)

                                //eventList.sortWith(compareBy({c.time}, {startDate.time}))

                                // create adapter and display events using the event list
                                arrayAdapterFBEvents = ArrayAdapter(
                                    this@EventRequestActivity,
                                    android.R.layout.simple_list_item_1,
                                    eventList
                                )

                                lstEvents.adapter = arrayAdapterFBEvents

                                lstEvents.onItemClickListener =
                                    AdapterView.OnItemClickListener { parent, v, position, id ->
                                        // on item click, create a dialog that handles attending and not attending functionality.
                                        val alert =
                                            AlertDialog.Builder(this@EventRequestActivity)

                                        // message is the event details from the list of event details above
                                        alert.setMessage(eventList[position])
                                        alert.setTitle("Attend this Event?")

                                        // attend button listener
                                        alert.setPositiveButton("Attend") { _, _ ->
                                            try {
                                                Toast.makeText(
                                                    this@EventRequestActivity,
                                                    "Attending Confirmed",
                                                    Toast.LENGTH_SHORT
                                                ).show()

                                                // update the 'attending' array in events document by pushing the users ID into it.
                                                db.collection("events").document(eventIDs[position])
                                                    .update(
                                                        "attending",
                                                        FieldValue.arrayUnion(userID)
                                                    )
                                                    .addOnSuccessListener {
                                                        Log.d(
                                                            SUCCESS,
                                                            "DocumentSnapshot successfully written!"
                                                        )
                                                    }
                                                    .addOnFailureListener { e ->
                                                        Log.w(
                                                            FAIL,
                                                            "Error writing document",
                                                            e
                                                        )
                                                    }
                                            } catch (e: Exception) {//on success and on fail listeners above handle the exceptions
                                            }

                                            // also update the soldiers 'attendingEvents' array with event ID
                                            db.collection("soldiers").document(userID)
                                                .update(
                                                    "attendingEvents",
                                                    FieldValue.arrayUnion(eventIDs[position])
                                                )
                                                .addOnSuccessListener {
                                                    Log.d(
                                                        SUCCESS,
                                                        "DocumentSnapshot successfully written!"
                                                    )
                                                }
                                                .addOnFailureListener { e ->
                                                    Log.w(
                                                        FAIL,
                                                        "Error writing document",
                                                        e
                                                    )
                                                }

                                            // Remove the event from the array adapter
                                            eventList.removeAt(position)
                                            eventIDs.removeAt(position)

                                            //notify adapter that there was a change in the list it is using.
                                            arrayAdapterFBEvents.notifyDataSetChanged()
                                        }

                                        alert.setNegativeButton("Not Attending") { _, _ ->
                                            // Not attending events get added to soldiers document
                                            db.collection("soldiers").document(userID)
                                                .update(
                                                    "notAttending",
                                                    FieldValue.arrayUnion(eventIDs[position])
                                                )
                                                .addOnSuccessListener {
                                                    Log.d(
                                                        SUCCESS,
                                                        "DocumentSnapshot successfully written!"
                                                    )
                                                }
                                                .addOnFailureListener { e ->
                                                    Log.w(
                                                        FAIL,
                                                        "Error writing document",
                                                        e
                                                    )
                                                }

                                            Toast.makeText(
                                                this@EventRequestActivity,
                                                "Not Attending",
                                                Toast.LENGTH_SHORT
                                            ).show()

                                            eventList.removeAt(position)
                                            eventIDs.removeAt(position)

                                            arrayAdapterFBEvents.notifyDataSetChanged()

                                        }

                                        alert.show()
                                    }
                            }
                        }
                    }
                }
            }
        }
    }

    fun onNotAttendingClick(view: View) {
        // Intent for the activity to open when user selects the notification
        val notAttendingIntent = Intent(
            this@EventRequestActivity,
            NotAttendingActivity::class.java
        )

        notAttendingIntent.putExtra("userID", userID)

        startActivity(notAttendingIntent)
    }
}

